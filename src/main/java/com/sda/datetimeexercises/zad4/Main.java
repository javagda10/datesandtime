package com.sda.datetimeexercises.zad4;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.Period;
import java.time.format.DateTimeFormatter;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy/MM/dd");
        String dataPierwsza = scanner.next();
        LocalDate dateStart = LocalDate.parse(dataPierwsza, formatter);

        String dataDruga = scanner.next();
        LocalDate dateStop = LocalDate.parse(dataPierwsza, formatter);

        Period okres = Period.between(dateStart, dateStop);
        System.out.println("Lat: " + okres.getYears() + " Miesiecy: " + okres.getMonths() + " Dni: " + okres.getDays());
    }
}
