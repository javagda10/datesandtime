package com.sda.datetimeexercises;

import java.time.Duration;
import java.time.LocalDate;
import java.time.LocalTime;
import java.time.Period;

public class Main {
    public static void main(String[] args) {
        LocalDate teraz = LocalDate.now();
        LocalDate grunwald = LocalDate.of(1410, 7, 15);

        Period okres = Period.between(grunwald, teraz);

        System.out.println(okres.getYears());

//        LocalTime time = LocalTime.now();
//        Duration czasTrwania = Duration.between(time, LocalTime.now());
    }
}
