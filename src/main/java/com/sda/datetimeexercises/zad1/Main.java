package com.sda.datetimeexercises.zad1;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);

        boolean isWorking = true;


        while (isWorking) {
            String coWypisac = sc.next();

            if (coWypisac.equals("date")) {
                System.out.println(LocalDate.now());
            } else if (coWypisac.equals("time")) {
                DateTimeFormatter formatter = DateTimeFormatter.ofPattern("HH:mm:ss");

                String sformatowanyCzas = LocalTime.now().format(formatter);

                System.out.println(sformatowanyCzas);
            } else if (coWypisac.equals("datetime")) {
                DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd/MM/yyyy HH:ss");

                String sformatowanaDataICzas = LocalDateTime.now().format(formatter);

                System.out.println(sformatowanaDataICzas);
            } else if (coWypisac.equals("quit")) {
                break;
            }
        }
    }
}
